var game = new Phaser.Game(800, 570, Phaser.AUTO, 'bon-courage');

game.state.add('Boot', BonCourage.Boot);
game.state.add('Preloader', BonCourage.Preloader);
game.state.add('MainMenu', BonCourage.MainMenu);
game.state.add('Game', BonCourage.Game);

game.state.start('Boot');