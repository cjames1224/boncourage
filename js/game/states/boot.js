var BonCourage = function(){

}

BonCourage.Boot = function(game){};

BonCourage.Boot.prototype = {
    
  preload: function() {
  
    this.load.image('splash', 'assets/images/logo.png');
    this.load.image('button', 'assets/images/button.png');
    this.load.image('preloaderBar', 'assets/images/preloader-bar.png');
    this.load.image('background', 'assets/images/background.png');
      
      
    this.load.image('button', 'assets/images/button.png');
    this.load.image('button2', 'assets/images/button2.png');
    this.load.image('button3', 'assets/images/button3.png');
    this.load.image('button4', 'assets/images/button4.png');
    this.load.image('button5', 'assets/images/button5.png');
    this.load.image('button6', 'assets/images/button6.png');
    this.load.image('button7', 'assets/images/button7.png');
    this.load.image('button8', 'assets/images/button8.png');
    this.load.image('button9', 'assets/images/button9.png');
  
  },
  create: function() {
      this.game.stage.backgroundColor = '#fff';
    
    //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
    this.input.maxPointers = 1;

    if (this.game.device.desktop) {
      //  If you have any desktop specific settings, they can go in here
      this.scale.pageAlignHorizontally = true;
    } else {
      //  Same goes for mobile settings.
      //  In this case we're saying "scale the game, no lower than 480x260 and no higher than 1024x768"
      
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.minWidth = 320;
      this.scale.minHeight = 240;
      this.scale.maxWidth = 640;
      this.scale.maxHeight = 480;
      this.scale.forceLandscape = true;
      this.scale.pageAlignHorizontally = true;
      this.scale.pageAlignVeritcally = true;
      this.scale.setScreenSize(true);
    }
   
    this.state.start('Preloader');   
  },
      
    
    
};