BonCourage.Preloader = function (game) {

  this.background = null;
  this.preloadBar = null;
  this.ready = false;

};

BonCourage.Preloader.prototype = {

  preload: function () {
      
      this.scale.pageAlignVeritcally = true;
      
      this.load.spritesheet('player', 'assets/images/player.png',60,75);
  this.background = this.add.sprite(this.game.width/2, this.game.height/2, 'splash');
    this.background.anchor.setTo(0.5, 0.5);
      
      this.preloadBar = this.add.sprite(this.game.width/2, (this.game.height/2 )+ 128, 'preloaderBar');
    this.preloadBar.anchor.setTo(0.5, 0.5);

    //  This sets the preloadBar sprite as a loader sprite.
    //  What that does is automatically crop the sprite from 0 to full-width
    //  as the files below are loaded in.
    this.load.setPreloadSprite(this.preloadBar);
   
      
      this.load.bitmapFont('font1', 'assets/fonts/font1/font1.png', 'assets/fonts/font1/font1.xml');
      this.load.bitmapFont('font2', 'assets/fonts/font2/font2.png', 'assets/fonts/font2/font2.xml');
    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
      
        game.load.text('txt','assets/prompts.txt');
  },
    
    create:function(){
this.preloadBar.cropEnabled = false;
        

    },

update:function(){
       if ( this.ready == true)
    {
      this.state.start('MainMenu');
    }
},
 onLoadComplete: function() {
    this.ready = true;
  }
};