BonCourage.Game = function(game) {
    console.log('init');
    // score
    this.totalScore = 0;
    this.score = 0;
    this.courageFactor = 1;
    this.promptCounter = 0;

    // settings
    this.playerMaxAngle = 20;
    this.playerMinAngle = -20;
    this.totalItems = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.itemsA = [1.4, 7.6, 22.4, 132.9, 281.4, 625.1, 1333.3, 3002.2, 8626.2, 77777.7];
    
    this.requirements = [0, 500, 10000, 50000, 100000, 2000000, 55000000, 500000000, 1000000000, 7777700000000000];
};

var ptm = null;

BonCourage.Game.prototype = {
    getItemPrice: function(num) {
        return Math.round(this.itemsA[num] + ((this.itemsA[num]) * (Math.pow(this.itemsA[num], this.totalItems[num]))));
    },

    create: function() {

        this.scale.pageAlignVeritcally = true;
        this.background = this.game.add.tileSprite(0, 0, this.game.width, 480, 'background');
        this.background.autoScroll(-325, 0);

        this.player = this.add.sprite(this.game.world.width / 2, 370, 'player');
        this.player.anchor.setTo(0.5);
        this.player.animations.add("run", [0, 1, 2, 3, 4, 5, 6]);
        this.player.animations.play('run', 10, true);

        this.scoreText = this.game.add.bitmapText(10, 10, 'font2', 'Courage\n' + this.score, 52);
        this.scoreText.align = 'center';

        this.player.inputEnabled = true;
        this.player.input.useHandCursor = true;
        this.player.events.onInputOver.add(function() {
            this.promptCounter++;
            this.createFadingText(this.courageFactor);

        }, this);

        this.fadingTexts = this.game.add.group();

        var pt = this.game.cache.getText('txt');
        this.promptText = pt.split('\n');
        this.generatePrompt();

/********** BUTTON 1 *********/
        this.store1button = this.game.add.button(25, 480, 'button', function() {
            if (this.score >= this.getItemPrice(0)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 2.5, Infinity, this.timerOne, this);
                this.score -= this.getItemPrice(0);
                this.totalItems[0]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);

        this.store1button.events.onInputOver.add(function() {
            if (this.cost1) {
                this.cost1.kill();
            }
            this.cost1 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(0), 48);

        }, this);
        this.store1button.events.onInputOut.add(function() {
            if (this.cost1) {
                this.cost1.kill();
                this.cost1 = null;
            }

        }, this);

/********** BUTTON 2 *********/
        this.store2button = this.game.add.button(175, 480, 'button2', function() {
            if (this.score >= this.getItemPrice(1)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 4, Infinity, this.timerTwo, this);
                this.score -= this.getItemPrice(1);
                this.totalItems[1]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        
        
        this.store2button.events.onInputOver.add(function() {
            if (this.cost2) {
                this.cost2.kill();
            }
            this.cost2 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(1), 48);

        }, this);
        this.store2button.events.onInputOut.add(function() {
            if (this.cost2) {
                this.cost2.kill();
                this.cost2 = null;
            }

        }, this);
        this.store2button.visible = false;
        
/********** BUTTON 3 *********/
        this.store3button = this.game.add.button(325, 480, 'button3', function() {
            if (this.score >= this.getItemPrice(2)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 7, Infinity, this.timerThree, this);
                this.score -= this.getItemPrice(2);
                this.totalItems[2]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        
        this.store3button.events.onInputOver.add(function() {
            if (this.cost3) {
                this.cost3.kill();
            }
            this.cost3 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(2), 48);

        }, this);
        this.store3button.events.onInputOut.add(function() {
            if (this.cost3) {
                this.cost3.kill();
                this.cost3 = null;
            }

        }, this);
        this.store3button.visible = false;
        
/********** BUTTON 4 *********/        
        this.store4button = this.game.add.button(475, 480, 'button4', function() {
            if (this.score >= this.getItemPrice(3)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 8, Infinity, this.timerFour, this);
                this.score -= this.getItemPrice(3);
                this.totalItems[3]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
         this.store4button.events.onInputOver.add(function() {
            if (this.cost4) {
                this.cost4.kill();
            }
            this.cost4 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(3), 48);

        }, this);
        this.store4button.events.onInputOut.add(function() {
            if (this.cost4) {
                this.cost4.kill();
                this.cost4 = null;
            }

        }, this);
        this.store4button.visible = false;
        
        
        
/********** BUTTON 5 *********/        
        this.store5button = this.game.add.button(625, 480, 'button5', function() {
            if (this.score >= this.getItemPrice(4)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 9, Infinity, this.timerFive, this);
                this.score -= this.getItemPrice(4);
                this.totalItems[4]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        this.store5button.events.onInputOver.add(function() {
            if (this.cost5) {
                this.cost5.kill();
            }
            this.cost5 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(4), 48);

        }, this);
        this.store5button.events.onInputOut.add(function() {
            if (this.cost5) {
                this.cost5.kill();
                this.cost5 = null;
            }

        }, this);
        this.store5button.visible = false;
        
/********** BUTTON 6 *********/ 
        this.store6button = this.game.add.button(100, 525, 'button6', function() {
            if (this.score >= this.getItemPrice(5)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 12, Infinity, this.timerSix, this);
                this.score -= this.getItemPrice(5);
                this.totalItems[5]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        this.store6button.events.onInputOver.add(function() {
            if (this.cost6) {
                this.cost6.kill();
            }
            this.cost6 = this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(5), 48);

        }, this);
        this.store6button.events.onInputOut.add(function() {
            if (this.cost6) {
                this.cost6.kill();
                this.cost6 = null;
            }

        }, this);
        this.store6button.visible = false;
        
        
        
/********** BUTTON 7 *********/ 
        this.store7button = this.game.add.button(250, 525, 'button7', function() {
            if (this.score >= this.getItemPrice(6)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 15, Infinity, this.timerSeven, this);
                this.score -= this.getItemPrice(6);
                this.totalItems[6]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        this.store7button.events.onInputOver.add(function() {
            if (this.cost7) {
                this.cost7.kill();
            }
            this.cost7= this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(6), 48);

        }, this);
        this.store7button.events.onInputOut.add(function() {
            if (this.cost7) {
                this.cost7.kill();
                this.cost7 = null;
            }

        }, this);
        this.store7button.visible = false;
        
        
        
/********** BUTTON 8 *********/ 
        this.store8button = this.game.add.button(400, 525, 'button8', function() {
            if (this.score >= this.getItemPrice(7)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 17, Infinity, this.timerEight, this);
                this.score -= this.getItemPrice(7);
                this.totalItems[7]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        this.store8button.events.onInputOver.add(function() {
            if (this.cost8) {
                this.cost8.kill();
            }
            this.cost8= this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(7), 48);

        }, this);
        this.store8button.events.onInputOut.add(function() {
            if (this.cost8) {
                this.cost8.kill();
                this.cost8 = null;
            }

        }, this);
        this.store8button.visible = false;
        
        
/********** BUTTON 9 *********/ 
        this.store9button = this.game.add.button(550, 525, 'button9', function() {
            if (this.score >= this.getItemPrice(8)) {
                this.game.time.events.repeat(Phaser.Timer.SECOND * 20, Infinity, this.timerNine, this);
                this.score -= this.getItemPrice(8);
                this.totalItems[8]++;
                this.scoreText.text = 'Courage\n' + this.score;
            }
        }, this);
        this.store9button.events.onInputOver.add(function() {
            if (this.cost9) {
                this.cost9.kill();
            }
            this.cost9= this.game.add.bitmapText(this.game.input.activePointer.position.x, this.game.input.activePointer.position.y, 'font2', 'Cost: ' + this.getItemPrice(8), 48);

        }, this);
        this.store9button.events.onInputOut.add(function() {
            if (this.cost9) {
                this.cost9.kill();
                this.cost9 = null;
            }

        }, this);
        this.store9button.visible = false;


    },
    timerOne: function() {
        this.createFadingText(1)
    },
    timerTwo: function() {
        this.createFadingText(10)
    },
    timerThree: function() {
        this.createFadingText(50)
    },
    timerFour: function() {
        this.createFadingText(100)
    },
    timerFive: function() {
        this.createFadingText(200)
    },
    timerSix: function() {
        this.createFadingText(500)
    },
    timerSeven: function() {
        this.createFadingText(1000)
    },
    timerEight: function() {
        this.createFadingText(5000)
    },
    timerNine: function() {
        this.createFadingText(100000)
    },

    generatePrompt: function() {
        console.log('changed prompt');
        if (ptm) ptm.kill();
        var pNum = Math.min(Math.floor(this.totalScore / 250) + Math.floor((Math.random() * this.totalScore) / 250), this.promptText.length - 1);
        ptm = this.game.add.bitmapText(0, this.background.height - 48, 'font2', this.promptText[pNum], 42);
        ptm.x += (this.game.width - ptm.width) / 2;
    },

    createFadingText: function(plusValue) {

        if (this.scoreText.scale.x > 1 || this.player.scale.x > 1) {
            this.scoreText.scale.x = 1;
            this.scoreText.scale.y = 1;
            this.player.scale.x = 1;
            this.player.scale.y = 1;
        }
        this.add.tween(this.scoreText.scale).to({
            x: 1.1,
            y: 1.1
        }, 30, Phaser.Easing.Linear.None, false, 0, 0, true).start();
        this.add.tween(this.player.scale).to({
            x: 1.2,
            y: 1.2
        }, 40, Phaser.Easing.Linear.None, false, 0, 0, true).start();

        this.score += plusValue;
        this.scoreText.text = 'Courage\n' + this.score;
        this.fadingText = this.game.add.bitmapText(this.player.x - this.player.width / 2 + (Math.random() * this.player.width / 2), this.player.y - this.player.height / 2, 'font2', '+' + Number(plusValue), 64 + Math.min(plusValue / 2, 75));
        this.fadingTexts.add(this.fadingText);
        this.game.add.tween(this.fadingText).to({
            x: this.player.x + ((Math.random() * 250) - 125),
            y: 0
        }, 2500, Phaser.Easing.Linear.None, true);
        this.game.add.tween(this.fadingText).to({
            alpha: 0
        }, 2500, Phaser.Easing.Linear.None, true);
        this.game.add.tween(this.fadingText.scale).to({
            x: 0,
            y: 0
        }, 2500, Phaser.Easing.Linear.None, true);
        this.totalScore += plusValue;
        console.log(this.totalScore);
        if (Math.random() > (1 * Math.pow(0.9999, this.promptCounter))) {
            this.generatePrompt();
            this.promptCounter = 0;
        }
    },

    update: function() {
        this.checkPointersPreviews();
        this.checkUnlockRequirement();
        
        
    },

    shutdown: function() {

    },

    render: function() {

    },
    checkUnlockRequirement:function(){
        if(this.store2button.visible== false && this.totalScore > this.requirements[1]){
            this.store2button.visible= true;
        }
        if(this.store3button.visible== false && this.totalScore > this.requirements[2]){
            this.store3button.visible= true;
        }
        if(this.store4button.visible== false && this.totalScore > this.requirements[3]){
            this.store4button.visible= true;
        }
        if(this.store5button.visible== false && this.totalScore > this.requirements[4]){
            this.store5button.visible= true;
        }
        if(this.store6button.visible== false && this.totalScore > this.requirements[5]){
            this.store6button.visible= true;
        }
        if(this.store7button.visible== false && this.totalScore > this.requirements[6]){
            this.store7button.visible= true;
        }
        if(this.store8button.visible== false && this.totalScore > this.requirements[7]){
            this.store8button.visible= true;
        }
        if(this.store9button.visible== false && this.totalScore > this.requirements[8]){
            this.store9button.visible= true;
        }
            
        
    },
    checkPointersPreviews:function(){
        if (this.cost1) {
            this.cost1.x = this.game.input.activePointer.position.x;
            this.cost1.y = this.game.input.activePointer.position.y;
        }
        if (this.cost2) {
            this.cost2.x = this.game.input.activePointer.position.x;
            this.cost2.y = this.game.input.activePointer.position.y;
        }
        if (this.cost3) {
            this.cost3.x = this.game.input.activePointer.position.x;
            this.cost3.y = this.game.input.activePointer.position.y;
        }
        if (this.cost4) {
            this.cost4.x = this.game.input.activePointer.position.x;
            this.cost4.y = this.game.input.activePointer.position.y;
        }
        if (this.cost5) {
            this.cost5.x = this.game.input.activePointer.position.x;
            this.cost5.y = this.game.input.activePointer.position.y;
        }
        if (this.cost6) {
            this.cost6.x = this.game.input.activePointer.position.x;
            this.cost6.y = this.game.input.activePointer.position.y;
        }
        if (this.cost7) {
            this.cost7.x = this.game.input.activePointer.position.x;
            this.cost7.y = this.game.input.activePointer.position.y;
        }
        if (this.cost8) {
            this.cost8.x = this.game.input.activePointer.position.x;
            this.cost8.y = this.game.input.activePointer.position.y;
        }
        if (this.cost9) {
            this.cost9.x = this.game.input.activePointer.position.x;
            this.cost9.y = this.game.input.activePointer.position.y;
        }
}


};