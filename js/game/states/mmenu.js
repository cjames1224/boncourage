BonCourage.MainMenu = function (game) {
  this.playButton = null;
  this.player = null;
  this.music = null;
  this.map = null;
  this.layer = null;
  this.background = null;
};



BonCourage.MainMenu.prototype = {

    
    
  create: function () {
   
      
      this.scale.pageAlignVeritcally = true;
      this.titleSprite = this.game.add.image(this.game.width/2, this.game.height/2, 'splash');
    this.titleSprite.anchor.setTo(0.5, 0.5);


    this.titleSprite.fixedToCamera = true;
      
      this.titleSprite.alpha = 0;
      
      this.startText = this.game.add.bitmapText(0,0, 'font1','click to start', 32);
    this.startText.x = this.game.width / 2  - this.startText.textWidth / 2;
    this.startText.y = this.titleSprite.y + this.titleSprite.height * 2 / 3;
      
      this.startText.alpha = 0;

    
      this.titleTween = this.add.tween(this.titleSprite).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true );
     this.titleTween.onComplete.add(function(){this.add.tween(this.startText).to( { alpha: 1 }, 3500, Phaser.Easing.Linear.None, true );}, this);
      
      
      
  },
    
    update: function () {
    if(this.game.input.activePointer.justPressed()) {
     // this.transitionPlugin.to('Game');
        this.game.state.start('Game');
    }
  },
    
    fadeInPrompt:function(){
    
    }
};